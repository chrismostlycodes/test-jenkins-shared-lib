#!/usr/bin/env groovy

/**
 * Deploy static assets to s3

 * @dependsOn
 *  aws [aws-cli]
 *
 */
def call(String environment) {
 sh """#!/bin/sh -l
   aws s3 sync dist s3://${env.S3_DEPLOY_BUCKET}-${environment} \
    --delete \
    --exclude 'index-*.html' \
    --exclude 'js/config/env.*.js'
  """
}
