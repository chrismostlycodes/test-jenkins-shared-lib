#!/usr/bin/env groovy

/**
 * Send Hipchat notification
 * @param message Message to send room
 * @param credentialsTokenId Credentials plugin token for hipchat access
 * @param room Hipchat room ID
 * @param color Message Color [GREEN, RED, ...]
 *
 */
def call(String message, String credentialsTokenId, String room, String color) {
 withCredentials([string(credentialsId: credentialsTokenId, variable: credentialsTokenId)]) {
    hipchatSend(
      color: color,
      message: message,
      room: room,
      token: "${env[credentialsTokenId]}", 
      sendAs: 'Jenkins Pipeline Notifier',
      server: 'api.hipchat.com',
      v2enabled: true,
      failOnError: false
    )
  }
}
