#!/usr/bin/env groovy

/**
 * Load Properties file into the current environment
 * @uses
*   env.WORKSPACE from jenkins environment
 *
 * @param file Properties file to load
 *
 */
def call(String file = 'config/jenkins/build.properties') {
  echo "Adding ${file} contents To Environment"
  def file_absolute="${env.WORKSPACE}/${file}"
  if (fileExists(file_absolute)) {
    def properties = readProperties(file: file_absolute)
    properties.each { key, val ->  env."$key" = "$val"}
  } else {
    error("File not found: ${file_absolute}")
  }
}
