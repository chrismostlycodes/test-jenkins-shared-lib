#!/usr/bin/env groovy

/**
 * Inject Git Metadata Vars
 * @uses
 *   env.GIT_PROJECT from jenkins environment
 *
 * @org github organization
 *
 */
def call(String org = "VerveWireless") {
    env.GIT_COMMIT = sh(
        script: 'git rev-parse --verify HEAD',
        returnStdout: true
    ).trim()
    env.GIT_COMMIT_SHORT = env.GIT_COMMIT.substring(0,7)
    env.GIT_PROJECT_URI = "https://github.com/${org}/${env.GIT_PROJECT}/commit/${env.GIT_COMMIT_SHORT}"
}
