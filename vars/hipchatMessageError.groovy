#!/usr/bin/env groovy

/**
 * Create HTML Message for Hipchat failure notification
 * @param stage Pipeline stage that failed
 *
 */
def call(String stage) {
  return """
    Pipeline <i>${env.JOB_NAME}</i> encountered a failure during the <strong>${stage}</strong>. <br/>
    <a href='${env.GIT_PROJECT_URI}' target='_BLANK'>Offending Commit</a> - 
    <a href='${env.RUN_DISPLAY_URL}?page=pipeline' target='_BLANK'>Pipeline Stage Overview</a>
  """
}
