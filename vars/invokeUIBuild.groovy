#!/usr/bin/env groovy

/**
 * Invoke UI Webpack Build
 * @dependsOn
 *  yarn
 *
 */
def call() {
 sh '''#!/bin/sh -l
    if ! [ -x "$(command -v yarn)" ]; then
      echo 'Error: yarn is not installed.'
      exit 1
    fi

    yarn build
  '''
}
