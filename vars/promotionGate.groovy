#!/usr/bin/env groovy

/**
 * Send Hipchat notification
 * @param environment Environment
 * @param time Promotion timeout
 *
 */
def call(String environment, Integer time) {
 // Track for aborting
  milestone 1
  // Approval Timeout
  timeout(time: time, unit: 'MINUTES') {
    // https://issues.jenkins-ci.org/browse/JENKINS-40594
    def inputResult = input(
      message: "Do you approve a ${environment} release?"
    )
  }
  // Kill input Job
  milestone 2
}
