#!/usr/bin/env groovy

/**
 * Create HTML Message for Hipchat build promotion notification
 * @param environment environment deploying to
 *
 */
def call(String environment) {
  return """
     Build [${env.BRANCH_NAME}-${env.GIT_COMMIT_SHORT}] has been promoted for a deployment to ${environment}
     <a href='${env.RUN_DISPLAY_URL}?page=changes' target='_BLANK'>Deployment Details</a>
  """
}
