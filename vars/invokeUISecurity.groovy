#!/usr/bin/env groovy

/**
 * Invoke NSP to run package security checks
 * @dependsOn
 *  yarn
 *
 */
def call() {
 sh '''#!/bin/sh -l
    if ! [ -x "$(command -v yarn)" ]; then
      echo 'Error: yarn is not installed.'
      exit 1
    fi

    yarn security
  '''
}
