#!/usr/bin/env groovy

/**
 * Invoke UI Webpack Build
 * @dependsOn
 *  yarn
 *
 * @param environment [qa, staging, production]
 *
 */
def call(String environment) {
  sh """#!/bin/sh -l
    #
    # @TODO: Multiline interpolation not happy with yarn conditional
    #
    yarn copy:config:${environment} 
    yarn copy:html:${environment}
   """
}
