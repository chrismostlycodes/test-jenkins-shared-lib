# Shared Jenkins groovy libraries for deploying UIs

@WIP

### Exposed Statics

- deployStaticS3
- hipchatMessageError
- hipchatMessagePromotion
- hipchatMessageRelease
- injectEnvProperties
- injectGitMetadata
- invokeUIBuild
- invokeUIInstall
- invokeUILint
- invokeUITest
- linkUIAssets
- notityHipchat
- promotionGate

